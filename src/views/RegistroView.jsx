import React, { useState } from "react";
import { Input } from "../components/Input";
import { useNavigate } from "react-router-dom";
import { useUsuarioStore } from "../store/useUsuario";

export function RegistroView() {
  const [campos, setCampos] = useState({
    nombre:'',
    email:'',
    password:''
  });  
  const [camposValidos, setCamposValidos] = useState({
    nombre: false,
    email:false, 
    password:false,
  });
  const navegacion = useNavigate();
  const usuarioStore = useUsuarioStore();

  const registrarUsuario = (e) => {
    e.preventDefault()
    const resultado = usuarioStore.registrar(campos)
    if(!resultado){
      alert('Error, el email ya esta registrado!')
      return;
    }
    alert('Usario registrado con exito');
    navegacion('/');
  }
  
  return <div style={{
    height: '100vh',
    display:"flex",
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
  }} >
    <form 
      className="card"
      onSubmit={registrarUsuario}>
      <h2 style={{textAlign:'center'}}>Registrarse</h2>
      <div style={{marginTop: 15, width:'90%'}}>
        <Input
          onChange={(text,valido) => {
            setCampos({...campos, nombre:text});
            setCamposValidos({...camposValidos, nombre: valido})
          }}
          value={campos.nombre}
          placeholder='Nombre Completo'
          validacion={/^[a-zA-Z]+( [a-zA-Z]+){0,2}$/}
          mensajeError="Debe ingresar un nombre válido"
        />
      </div>
      <div style={{marginTop: 15, width:'90%'}}>
        <Input
          onChange={(text,valido) => {
            setCampos({...campos, email:text});
            setCamposValidos({...camposValidos, email: valido})
          }}
          value={campos.email}
          placeholder='Email'
          validacion={/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/}
          mensajeError="El email no es válido"
        />
      </div>
      <div style={{marginTop: 15, marginBottom: 15, width:'90%'}}>
        <Input
          type="password"
          onChange={(text,valido) => {
            setCampos({...campos, password:text});
            setCamposValidos({...camposValidos, password: valido})
          }}
          value={campos.password}
          placeholder='Contraseña'
          validacion={/^[a-zA-Z0-9._%+-]{8,}$/}
          mensajeError="La contraseña debe tener al menos 8 caracteres"
        />
      </div>
      <button 
        disabled={!camposValidos.email || !camposValidos.password || !camposValidos.nombre}
        style={{width:'100%', marginTop:10}} type="submit">
        Registrarse
      </button>
      <div style={{paddingTop:10}}>
        <a onClick={()=> navegacion('/')}>Regresar al login</a>
      </div>
    </form>
  </div>;
}

