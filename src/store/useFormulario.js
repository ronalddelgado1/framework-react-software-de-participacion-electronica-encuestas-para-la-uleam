import { create } from 'zustand'
import { persist } from 'zustand/middleware'
import {v4 as uuid} from "uuid";

export const useFormularioStore = create(
  persist((set, get) => ({
    formularioActual: null,
    formularios: [],
    agregar:(formulario)=> {
      formulario.id = uuid();
      set({formularios: [...get().formularios, formulario]})
    },
    eliminar:(id)=>{
      const formularios = get().formularios.filter(x => x.id !== id)
      set({formularios: formularios});
    },
    setFormularioActual:(formulario)=> set({formularioActual: formulario}),
  }),{
    name:'@formulario-storage',
  })
)