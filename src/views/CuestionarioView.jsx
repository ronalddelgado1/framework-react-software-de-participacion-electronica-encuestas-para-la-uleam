import React, { useState } from "react";
import { useFormularioStore } from "../store/useFormulario";
import { useNavigate } from "react-router-dom";
import { Navbar } from "../components/Navbar";

export function CuestionarioView() {
  const {formularioActual} = useFormularioStore();
  const [preguntas, setPreguntas] = useState(formularioActual.preguntas.map(x => ({...x, valor:null})));
  const navegacion = useNavigate();
  const responderPregunta = (indicePregunta, indiceOpcion)=>{
    preguntas[indicePregunta].valor = indiceOpcion;
    setPreguntas([...preguntas]);
  }

  return <div>
    <Navbar/>
    <form 
    onSubmit={(e)=> {
      e.preventDefault();
      let preguntasCorrectas = 0;
      for (let i = 0; i < preguntas.length; i++) {
        if(preguntas[i].opciones[preguntas[i].valor].respuesta){
          preguntasCorrectas++;
        }        
      }
      alert(`Puntaje ${preguntasCorrectas}/${preguntas.length}`);
      navegacion('/formularios')
    }}
    style={{
      padding: 20, 
      paddingLeft:80, 
      paddingRight: 80}}>
      <h2>{formularioActual.titulo}</h2>
      <hr />

      <div style={{
        marginTop: 20
      }}>
        {preguntas.map((x, i) => (
          <div key={i}>
            <h3>{x.id}. {x.pregunta}</h3>
            <div style={{marginTop:10, marginLeft:10}}>
              {x.opciones.map((opcion, j)=>(
                <div 
                  key={i+'-'+j}
                  style={{display:'flex'}}
                  >
                  <input 
                    id={i+'-'+j}
                    type="radio" 
                    value={j}
                    onChange={(e)=>{
                      responderPregunta(i,parseInt(e.target.value));
                    }}
                    name={x.pregunta}/>
                    <label
                      for={i+'-'+j}
                      style={{marginLeft: 5}}>{opcion.titulo}</label>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>

      <div style={{marginTop: 20, marginBottom: 20}}>
        <button 
          disabled={preguntas.find(x => x.valor=== null)}
          style={{width:'100%',height: 40}}
          type="submit">
          Enviar respuestas
        </button>
      </div>
    </form>
  </div>;
}

