import React from "react";
import { Navbar } from "../components/Navbar";
import { CardForm } from "../components/CardForm";
import { useNavigate } from "react-router-dom";
import { useFormularioStore } from "../store/useFormulario";

export function FormulariosView() {
  const {formularios} = useFormularioStore();
  const navegacion = useNavigate();

  return <div>
    <Navbar/>
    <div style={{padding: 20}}>
      <button 
        onClick={()=> navegacion('/formularios/nuevo')}
        style={{margin:5}}>
        Agregar Formulario
      </button>

      <div style={{
        display:'flex',
        flexDirection:'row',
        flexWrap:'wrap',
        marginTop: 20,
      }}>
        {formularios.map((x,i)=> (
          <CardForm 
            key={i}
            formulario={x} />
        ))}

      </div>
    </div>
  </div>;
}
