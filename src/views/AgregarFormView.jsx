import React, { useEffect, useState } from "react";
import { Navbar } from "../components/Navbar";
import { Input } from "../components/Input";
import { AiFillCheckCircle, AiFillCloseCircle, AiOutlineClose, AiOutlineCloseCircle, AiOutlineDelete } from "react-icons/ai";
import { useFormularioStore } from "../store/useFormulario";
import { useNavigate } from "react-router-dom";
import { useUsuarioStore } from "../store/useUsuario";


export function AgregarFormView() {
  const {usuarioActual} = useUsuarioStore();
  const [form, setForm] = useState({
    creador: usuarioActual?.email,
    titulo: '',
    preguntas:[
      {
        id: 1,
        pregunta: '',
        opciones:[
          {
            respuesta: true,
            titulo:''
          },
          {
            respuesta: false,
            titulo:''
          },
          {
            respuesta: false,
            titulo:''
          }
        ]
      }
    ]
  });
  const {agregar:agregarForm} = useFormularioStore();
  const navegacion = useNavigate();

  const agregarPregunta = ()=>{
    form.preguntas.push({
      id: form.preguntas.length+1,
      pregunta: '',
      opciones:[
        {
          respuesta: true,
          titulo:''
        },
        {
          respuesta: false,
          titulo:''
        },
        {
          respuesta: false,
          titulo:''
        }
      ]
    })
    setForm({...form});
  }
  
  const eliminarPregunta = (indice)=>{
    form.preguntas = form.preguntas.filter((x, i) => i !== indice);
    form.preguntas = form.preguntas.map((x,i) => ({...x, id:i+1}));
    setForm({...form})
  }


  return <div>
    <Navbar/>
    <form 
      onSubmit={(e)=> {
        e.preventDefault();
        agregarForm(form);
        navegacion('/formularios')
      }}
      style={{
      padding: 20, 
      paddingLeft:80, 
      paddingRight: 80}}>
      <h2>Agregar Formulario</h2>
      <hr />
      <Input 
        styles={{fontSize:25, width: '95%'}}
        placeholder={'Titulo del formulario'}
        value={form.titulo}
        onChange={(texto, valido)=> {
          setForm({...form, titulo: texto});
        }}
        required={true}
      />
      <div style={{
        marginTop: 20
      }}>
        {form.preguntas.map((x,i) => (
          <div key={i} 
          style={{
            marginTop: 30
          }}>
            <div style={{
              display:'flex',
              alignItems:1
            }}>
              <span style={{
                marginRight: 10
              }}>
                {x.id}.</span>
              <div style={{flex:10}}>
                <Input
                  value={form.preguntas[i].pregunta}
                  placeholder={'Pregunta '+x.id}
                  onChange={(texto)=>{
                    form.preguntas[i].pregunta = texto;
                    setForm({...form});
                  }}
                  styles={{padding:5, width: '95%'}}
                  required={true}
                />
              </div>
              {i !== 0 && <span 
                title="Eliminar pregunta"
                onClick={()=> eliminarPregunta(i)}
                style={{cursor:'pointer',flex:1}}>
                <AiOutlineDelete size={24}/>
              </span>}
            </div>
            <div style={{marginTop:10, marginLeft:18}}>
              {x.opciones.map((opcion, j) => (
                <div 
                  key={i+'-'+j}
                  style={{
                    display:'flex',
                    marginTop: 10
                  }}
                  >
                    {opcion.respuesta ? 
                      <span
                        title="Respuesta correcta">
                        <AiFillCheckCircle
                          size={24} 
                          style={{
                          cursor:'pointer'
                        }}/>
                      </span>                    
                      :
                      <span onClick={()=>{
                        form.preguntas[i].opciones = form.preguntas[i].opciones.map((item, k) => ({...item, respuesta: k == j}));
                        setForm({...form});
                      }}>
                        <AiOutlineCloseCircle
                          size={24} 
                          style={{
                          cursor:'pointer'
                        }}/>
                      </span>
                    }
                    <div style={{flex: 1, paddingLeft:10}}>
                      <Input 
                        placeholder={'Opcion '+(j+1)}
                        onChange={(texto)=> {
                          form.preguntas[i].opciones[j].titulo = texto;
                          setForm({...form})
                        }}
                        value={form.preguntas[i].opciones[j].titulo}
                        styles={{padding: 5, width: '85%'}}
                        required={true}
                      />
                    </div>
                </div>
              ))}
            </div>
          </div>
        ))}
      </div>

      <button 
        type="button"
        style={{marginTop:20}}
        onClick={()=>agregarPregunta()}>Agregar pregunta</button>
    
    <div style={{marginTop: 20, marginBottom: 20}}>
      <button 
        style={{width:'100%',height: 40}}
        type="submit">
        Guardar
      </button>
    </div>
    </form>

  </div>;
}

