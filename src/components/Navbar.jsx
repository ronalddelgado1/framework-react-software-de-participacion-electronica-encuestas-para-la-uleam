import React from "react";
import { useUsuarioStore } from "../store/useUsuario";
import { BiLogOut } from "react-icons/bi";
import { useNavigate } from "react-router-dom";

export function Navbar() {
  const {usuarioActual, cerrarSesion} = useUsuarioStore();
  const navegacion = useNavigate();

  
  return <div style={{
    backgroundColor:'#1C2128',
    height: 30,
    display: "flex",
    flexDirection: "row",
    justifyContent:'space-between',
    alignItems:'center',
    padding: 10,
    paddingRight: 30,
  }}>
    <div style={{flex: 1}}>
      <h4 
        style={{cursor:'pointer', display:'inline-block'}}
        onClick={()=>navegacion('/formularios')}>React Forms</h4>
    </div>
    <div style={{display:'flex', alignItems:'center'}}>
      <span
        style={{marginRight: 15}}>Hola {usuarioActual?.nombre}</span>
      <button style={{
        backgroundColor: '#1C2128',
        borderColor:'#444C56',
        padding:5
      }}
      onClick={()=> {
        cerrarSesion();
        navegacion('/')}}
      title="Cerrar sesión">
        <BiLogOut
          size={25}/> 
      </button>
    </div>
  </div>;
}
