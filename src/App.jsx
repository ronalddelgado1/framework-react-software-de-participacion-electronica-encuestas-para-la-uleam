import { BrowserRouter, Navigate, Outlet, Route, Routes } from 'react-router-dom'
import { LoginView } from './views/LoginView'
import { RegistroView } from './views/RegistroView'
import { FormulariosView } from './views/FormulariosView'
import { useUsuarioStore } from './store/useUsuario'
import { AgregarFormView } from './views/AgregarFormView'
import { CuestionarioView } from './views/CuestionarioView'

function App() {
  const {usuarioActual} = useUsuarioStore();
  
  return <BrowserRouter>
    <Routes>
      
      <Route path='/' element={!usuarioActual ? <Outlet/> : <Navigate to='/formularios' replace />}>
        <Route index element={<LoginView/>}/>
        <Route path='registro' element={<RegistroView/>}/>
      </Route>
       
      <Route 
          path="/formularios"
          element={usuarioActual ? <Outlet/> : <Navigate to='/' replace />}>    
          <Route index element={<FormulariosView/>}/>
          <Route path='nuevo' element={<AgregarFormView />}/>
          <Route path='resolver' element={<CuestionarioView />}/>
        </Route> 
    </Routes>
  </BrowserRouter>
}

export default App
