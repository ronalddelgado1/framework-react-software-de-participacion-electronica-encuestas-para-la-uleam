import { create } from 'zustand'
import { persist } from 'zustand/middleware'

export const useUsuarioStore = create(
  persist((set, get) => ({
    usuarioActual: null,
    usuarios: [],
    registrar:(usuario)=> {
      if(get().usuarios.find(x => x.email.toLocaleLowerCase() === usuario.email.toLocaleLowerCase())){
        return false;
      }
      set({usuarios: [...get().usuarios, usuario]})
      return true;
    },
    login:(credenciales)=>{
      const usuario = get().usuarios.find(x => credenciales?.email?.toLocaleLowerCase() === x.email && credenciales.password === x.password)
      if(usuario){
        set({usuarioActual: usuario});
        return true;
      }
      return false;
    },
    cerrarSesion:()=> set({usuarioActual: null}),
  }),{
    name:'@usuario-storage',
  })
)