import React from "react";
import fondo from "../assets/fondoCard.png";
import { AiFillDelete, AiFillPlayCircle, AiOutlineDelete } from "react-icons/ai";
import { useFormularioStore } from "../store/useFormulario";
import { useNavigate } from "react-router-dom";

export function CardForm({formulario}) {
  const {eliminar: eliminarForm, setFormularioActual} = useFormularioStore();
  const navegacion = useNavigate();

  return <div style={{
    backgroundColor:'#03787C',
    width: 250,
    display:'flex',
    flexDirection: 'column',
    margin: 5,
  }}>
    <div>
      <img width={'auto'} src={fondo} alt="fondo" />
    </div>
    <div style={{padding:10}}>
      <h3 style={{margin:0}}>{formulario?.titulo}</h3>
      <p style={{margin:0}}>{formulario?.creador}</p>
      <p style={{margin:0}}>Preguntas: {formulario?.preguntas?.length}</p>
      <div style={{display:"flex", justifyContent:'space-between'}}>
        <span 
          title="Empezar"
          style={{cursor:'pointer'}}
          onClick={()=> {
            setFormularioActual(formulario);
            navegacion('/formularios/resolver')
          }}>
          <AiFillPlayCircle size={24}/>
        </span>
        <span 
          title="Eliminar"
          style={{cursor:'pointer'}}
          onClick={()=> eliminarForm(formulario.id)}>
          <AiFillDelete size={24}/>
        </span>
      </div>
    </div>
  </div>;
}
