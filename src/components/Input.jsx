import React, { useState } from "react";

export function Input({
  type='text',
  onChange=(text,invalido)=>{},
  value,
  placeholder,
  validacion=null,
  mensajeError='Datos incorrectos',
  styles = {},
  required = null
}) {
  const [invalido, setInvalido] = useState(false);
  
  return <div>
    <input 
      style={{
        padding:10,
        width:'100%',
        fontSize: 14,
        ...styles,
      }}
      type={type}
      placeholder={placeholder}
      onChange={(e)=>{
        if(validacion && !validacion.test(e.target.value)){
          setInvalido(true)
          onChange(e.target.value, false)
        }else{
          setInvalido(false)
          onChange(e.target.value, true)
        }
      }}
      value={value}
      required={!!required}
    />
    {invalido && 
      <p style={{
        margin: 0,
        fontSize: 12,
        color: 'rgb(229 83 75)'
      }}>{mensajeError}</p>}
  </div>;
}
