import React, { useState } from "react";
import { Input } from "../components/Input";
import { useNavigate } from "react-router-dom";
import { useUsuarioStore } from "../store/useUsuario";

export function LoginView() {
  const [campos, setCampos] = useState({
    email:'',
    password:''
  });  
  const [camposValidos, setCamposValidos] = useState({email:false, password:false});
  const navegacion = useNavigate();
  const usuarioStore = useUsuarioStore();
  
  const iniciarSesion = (e)=>{
    e.preventDefault();
    const resultado = usuarioStore.login(campos);
    if(!resultado){
      alert('Error, credenciales incorrectas!');
      return;
    }
    navegacion('/formularios');
  }

  return <div style={{
    height: '100vh',
    display:"flex",
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
  }} >
    <form 
      className="card"
      onSubmit={iniciarSesion}>
      <h2 style={{textAlign:'center'}}>Login</h2>
      <div style={{marginTop: 15, width:'90%'}}>
        <Input
          onChange={(text,valido) => {
            setCampos({...campos, email:text});
            setCamposValidos({...camposValidos, email: valido})
          }}
          value={campos.email}
          placeholder='Email'
          validacion={/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/}
          mensajeError="El email no es válido"
        />
      </div>
      <div style={{marginTop: 15, marginBottom: 15, width:'90%'}}>
        <Input
          type="password"
          onChange={(text,valido) => {
            setCampos({...campos, password:text});
            setCamposValidos({...camposValidos, password: valido})
          }}
          value={campos.password}
          placeholder='Contraseña'
          validacion={/^[a-zA-Z0-9._%+-]{8,}$/}
          mensajeError="La contraseña debe tener al menos 8 caracteres"
        />
      </div>
      <button 
        disabled={!camposValidos.email || !camposValidos.password}
        style={{width:'100%', marginTop:10}} type="submit">
        Ingresar
      </button>
      <div style={{paddingTop:10}}>
        <a onClick={()=> navegacion('/registro')}>¿No tiene una cuenta?</a>
      </div>
    </form>
  </div>;
}

